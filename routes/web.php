<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::group(['middleware'=>['auth']],function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //****** Accounts ***********//
    Route::prefix(config('app.account'))->group(function () {
        Route::resource('account-type', 'App\Http\Controllers\AccountTypeController');
        Route::resource('bank-account', 'App\Http\Controllers\BankAccountController');
        Route::get('bank-deposit/{id}', 'App\Http\Controllers\BankDepositController@bankDeposit');
        Route::resource('bank-deposit', 'App\Http\Controllers\BankDepositController');
        Route::get('amount-transfer/{id}', 'App\Http\Controllers\AmountTransferController@amountTransfer');
        Route::resource('amount-transfer', 'App\Http\Controllers\AmountTransferController');
        Route::get('amount-withdraw/{id}', 'App\Http\Controllers\AmountWithdrawController@amountWithdraw');
        Route::resource('amount-withdraw', 'App\Http\Controllers\AmountWithdrawController');
        Route::get('bank-report/{id}', 'App\Http\Controllers\BankAccountController@showBankReport');
        Route::post('bank-report/{id}', 'App\Http\Controllers\BankAccountController@showBankReportFilter')->name('bank-report.filter');
        Route::post('find-chequeno-with-chequebook-id', 'App\Http\Controllers\AmountWithdrawController@findChequeNoWithChequeBookId');
        Route::resource('cheque-book', 'App\Http\Controllers\ChequeBookController');
        Route::resource('cheque-no', 'App\Http\Controllers\ChequeNoController');
        Route::resource('daily-transaction', 'App\Http\Controllers\DailyTransactionController');
        Route::post('daily-transaction', 'App\Http\Controllers\DailyTransactionController@filter')->name('transaction.filter');
        Route::get('final-report', 'App\Http\Controllers\DailyTransactionController@finalReport');
        Route::post('final-report', 'App\Http\Controllers\DailyTransactionController@finalReportFiltering')->name('final-report.filter');
    });

    //******** Other Receive *******//
    Route::prefix(config('app.or'))->group(function () {
        Route::resource('receive-type', 'App\Http\Controllers\ReceiveTypeController');
        Route::resource('receive-sub-type', 'App\Http\Controllers\ReceiveSubTypeController');
        Route::resource('receive-voucher', 'App\Http\Controllers\ReceiveVoucherController');
        Route::get('receive-voucher-report', 'App\Http\Controllers\ReceiveVoucherController@report');
        Route::post('receive-voucher-report', 'App\Http\Controllers\ReceiveVoucherController@filter')->name('receive.filter');
        Route::post('find-receive-subtype-with-type-id', 'App\Http\Controllers\ReceiveVoucherController@findReceiveSubTypeWithType');
    });

    //******** Other Payment *******//
    Route::prefix(config('app.op'))->group(function () {
        Route::resource('payment-type', 'App\Http\Controllers\PaymentTypeController');
        Route::resource('payment-sub-type', 'App\Http\Controllers\PaymentSubTypeController');
        Route::resource('payment-voucher', 'App\Http\Controllers\PaymentVoucherController');
        Route::get('payment-voucher-report', 'App\Http\Controllers\PaymentVoucherController@report');
        Route::post('payment-voucher-report', 'App\Http\Controllers\PaymentVoucherController@filter')->name('payment.filter');
        Route::post('find-payment-subtype-with-type-id', 'App\Http\Controllers\PaymentVoucherController@findPaymentSubTypeWithType');
    });

    //******** customer management *******//
    Route::prefix(config('app.customer'))->group(function () {
        Route::get('customer', 'App\Http\Controllers\CustomerController@index');
        Route::get('customer-search', 'App\Http\Controllers\CustomerController@customerFilter')->name('customers.search');
        Route::get('customer/create', 'App\Http\Controllers\CustomerController@create');
        Route::post('store-customer', 'App\Http\Controllers\CustomerController@store')->name('store-customer');
        //Route::get('customer/{id}/edit', 'App\Http\Controllers\CustomerController@edit')->name('customer.edit');
        Route::put('update-customer/{id}', 'App\Http\Controllers\CustomerController@update')->name('update-customer');
        Route::delete('customer-destroy/{id}', 'App\Http\Controllers\CustomerController@destroy')->name('customer-destroy');
        Route::get('customer-ledger/{id}', 'App\Http\Controllers\CustomerController@customerLedger');
    });

    //****** Products ******//
    Route::prefix(config('app.product'))->group(function () {
        Route::resource('product-type', 'App\Http\Controllers\ProductTypeController');
        Route::resource('product-sub-type', 'App\Http\Controllers\ProductSubTypeController');
        Route::resource('product-unit', 'App\Http\Controllers\ProductUnitController');
        Route::resource('product-brand', 'App\Http\Controllers\ProductBrandController');
        Route::resource('product', 'App\Http\Controllers\ProductController');
        Route::post('find-product-subtype-with-type-id', 'App\Http\Controllers\ProductController@findProductSubTypeWithType');
        Route::get('barcode/{id}', 'App\Http\Controllers\BarcodeGeneratorController@index');
        Route::resource('stock-product', 'App\Http\Controllers\StockProductController');
        Route::get('stock-product-export', 'App\Http\Controllers\StockProductController@export');
        Route::resource('update-stock-product', 'App\Http\Controllers\UpdateStockProductController');
    });

    // ********* Supplier ****** //
    Route::prefix(config('app.supplier'))->group(function () {
        Route::resource('product-supplier', 'App\Http\Controllers\ProductSupplierController');
        Route::get('product-supplier-payment', 'App\Http\Controllers\ProductSupplierController@supplierList');
        Route::get('product-supplier-payment-report', 'App\Http\Controllers\ProductSupplierController@supplierPaymentReport');
        Route::post('product-supplier-payment-report', 'App\Http\Controllers\ProductSupplierController@filter')->name('suplier-pay-report.filter');
        Route::resource('supplier-area', 'App\Http\Controllers\ProductSupplierAreaController');
        Route::get('supplier-ledger/{id}', 'App\Http\Controllers\ProductSupplierController@supplierLedger');
    });
});
