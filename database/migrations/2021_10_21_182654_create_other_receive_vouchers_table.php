<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherReceiveVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_receive_vouchers', function (Blueprint $table) {
            $table->id();
            $table->integer('receive_type_id')->nullable();
            $table->integer('receive_sub_type_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('receive_from')->nullable();
            $table->double('amount')->nullable();
            $table->date('receive_date')->nullable();
            $table->string('issue_by')->nullable();
            $table->string('note')->nullable();
            $table->boolean('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('tok')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_receive_vouchers');
    }
}
