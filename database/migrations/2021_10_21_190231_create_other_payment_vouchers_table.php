<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherPaymentVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_payment_vouchers', function (Blueprint $table) {
            $table->id();
            $table->integer('payment_type_id')->nullable();
            $table->integer('payment_sub_type_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('payment_for')->nullable();
            $table->double('amount')->nullable();
            $table->date('payment_date')->nullable();
            $table->string('issue_by')->nullable();
            $table->string('note')->nullable();
            $table->boolean('status')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('tok')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_payment_vouchers');
    }
}
