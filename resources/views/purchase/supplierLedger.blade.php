@extends('layouts.layout')
@section('title', 'Supplier Ledger')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> {{ __('messages.supplier_ledger') }} <small></small> </h1>
  <ol class="breadcrumb">
    <li><a href="{{URL::To('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Ledger</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  @include('common.message')
  @include('common.commonFunction')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"> <i class="fa fa-list-alt"></i> {{ __('messages.supplier_ledger') }}</h3>
          <a href="javascript:void(0)" onclick="PrintElem('#mydiv')" class="btn btn-info btn-sm pull-right"><i class="fa fa-print"></i></a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12" id="mydiv">
              <!--style>
                table {
                  border-collapse: collapse;
                }
                table, th, td {
                  border: 1px solid black;
                }
              </style-->
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-responsive table-hover" width="100%" style="margin-left: auto; margin-right: auto;">
                  <thead>
                    <tr> 
                      <td>{{ __('messages.supplier') }}</td>
                      <td>{{$singledata->name}}</td>
                      <td>{{ __('messages.supplier') }} {{ __('messages.id') }}</td>
                      <td>{{$singledata->supplier_id}}</td>
                    </tr>
                    <tr> 
                      <td>{{ __('messages.email') }}</td>
                      <td>{{$singledata->email}}</td>
                      <td>{{ __('messages.phone') }}</td>
                      <td>{{$singledata->phone}}</td>
                    </tr>
                  </thead>
                </table>
                <table class="table table-bordered table-striped table-responsive table-hover" width="100%" style="margin-left: auto; margin-right: auto;"> 
                  <thead> 
                    <tr>
                      <th style="text-align: left">{{ __('messages.SL') }}</th>
                      <th style="text-align: left">{{ __('messages.date') }}</th>
                      <th style="text-align: left">{{ __('messages.reason') }}</th>
                      <th style="text-align: left">{{ __('messages.credit') }}</th>
                      <th style="text-align: left">{{ __('messages.debit') }}</th>
                      <th style="text-align: left">{{ __('messages.balance') }}</th>
                    </tr>
                  </thead>
                  <tbody> 
                    <?php                           
                      $number = 1;
                      $numElementsPerPage = 15; // How many elements per page
                      $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                      $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                      $rowCount = 0;

                      $credit = 0;
                      $debit = 0;
                      $sum = 0;
                    ?>
                    @foreach($alldata as $data)
                      <?php 
                        $rowCount++;
                      ?>
                    <tr>
                      <td>{{$currentNumber++}}</td>
                      <td>{{dateFormateForView($data->date)}}</td>
                      <td>{{ucfirst($data->reason)}}</td>
                      <td>
                        <?php
                          $reasons = $data->reason;

                          if(preg_match("/purchase/", $reasons)) {
                            echo $data->amount;
                            $sum = $sum+$data->amount;
                            $credit = $credit+$data->amount;
                          }
                        ?>
                      </td>
                      <td>
                        <?php
                          $reasons = $data->reason;

                          if(preg_match("/payment/", $reasons)) {
                            echo $data->amount;
                            $sum = $sum-$data->amount;
                            $debit = $debit+$data->amount;
                          }
                        ?>
                      </td>
                      <td>{{$sum}}</td>
                    </tr>
                    @endforeach
                    @if($rowCount==0)
                      <tr>
                        <td colspan="6" align="center">
                          <h4 style="color: #ccc">No Data Found . . .</h4>
                        </td>
                      </tr>
                    @endif
                  </tbody>
                  <tfoot>
                    
                  </tfoot>
                </table>
                <div class="col-md-12" align="right"></div>
              </div>
            </div>
            <div class="col-md-12"> 
              <!-- <input type="button" value="Print" onclick="PrintElem('#mydiv')" class="btn-success" /> -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer"></div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<script type="text/javascript">
  <?php
    $siteInfo = DB::table('site_setting')->where('id', 1)->first();
    $header = '';
    $logo = '';
    if (!empty($siteInfo)) {
      $logo = '../public/storage/app/public/uploads/logo/'.$siteInfo->image;

      $header = '<td width="20%"><img src="'.$logo.'" width="70px" height="70px"></td><td width="80%"><center><h3 style="margin:0; padding:0; margin-bottom: 0px">'.$siteInfo->company_name.'</h3>'.$siteInfo->address.'<br> Mobile : '.$siteInfo->phone.', Email : '.$siteInfo->email.'</center></td>';
    }
  ?>

  function PrintElem(elem)
  {
    Popup($(elem).html());
  }

  function Popup(data) 
  {   
    var mywindow = window.open('', 'my div', 'height=1000,width=1000');
    var is_chrome = Boolean(mywindow.chrome);
    mywindow.document.write('<html><head><title>Binary IT</title><style>a {text-decoration:none;}</style>');

    var Header = '<?php echo $header;?>';

    mywindow.document.write('<table width="80%" style="margin-left: auto; margin-right: auto"><tr>'+Header+'</tr></table><br>');
    
    mywindow.document.write('</head><body>');
    mywindow.document.write('<center><u><span style="font-weight: bold; font-size: 15px">Supplier Ledger</span></u><center>');    
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
      
    if (is_chrome) {
      setTimeout(function() { // wait until all resources loaded 
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10
      mywindow.print(); // change window to winPrint
      mywindow.close(); // change window to winPrint
      }, 500);
    } else {
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10

      mywindow.print();
      mywindow.close();
    }
    return true;
  }
</script>
@endsection 