@extends('layouts.layout')
@section('title', 'Purchase Report')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> PURCHASE DETAIL REPORT<small></small> </h1>
  <ol class="breadcrumb">
    <li><a href="{{URL::To('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
  </ol>
</section>
<style type="text/css">
  .borderless td, .borderless th {
    border: none!important;
  }
</style>
<!-- Main content -->
<section class="content">
  @include('common.message')
  @include('common.commonFunction')
  <div class="row">
    <div class="col-md-12" id="">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"> <i class="fa fa-list-alt"></i> PURCHASE DETAIL REPORT</h3>
          <div class="form-inline pull-right">
            <div class="input-group">
              <a href="javascript:void(0)" onclick="PrintElem('#mydiv')" class="btn btn-info btn-xs"><i class="fa fa-print"></i></a>
            </div>
            <div class="input-group">
              <a href="{{URL::To('product-purchase')}}" class="btn btn-success btn-xs"><i class="fa fa-get-pocket"></i> Purchase Product</a>
            </div>
            <div class="input-group">
              <a href="{{URL::To('purchase-report')}}" class="btn btn-success btn-xs"><i class="fa fa-list-alt"></i> Purchase Report</a>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12" id="mydiv">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-responsive table-hover" width="100%" style="margin-left: auto; margin-right: auto;">
                  <thead>
                    <tr> 
                      <td>Supplier Name</td>
                      <td>{{$singledata->productpurchase_supplier_object->name}}</td>
                      <td>Supplier ID</td>
                      <td>{{$singledata->productpurchase_supplier_object->supplier_id}}</td>
                    </tr>
                    <tr> 
                      <td>Email</td>
                      <td>{{$singledata->productpurchase_supplier_object->email}}</td>
                      <td>Phone</td>
                      <td>{{$singledata->productpurchase_supplier_object->phone}}</td>
                    </tr>
                    <tr> 
                      <td>Purchase Date</td>
                      <td>{{$singledata->purchase_date}}</td>
                      <td>Invoice</td>
                      <td>{{$singledata->tok}}</td>
                    </tr>
                  </thead>
                </table>
                <table class="table table-bordered table-striped table-responsive table-hover" width="100%" style="margin-left: auto; margin-right: auto;"> 
                  <thead> 
                    <tr>
                      <th style="text-align: left">SL</th>
                      <th style="text-align: left">Product</th>
                      <th style="text-align: left">Quantity</th>
                      <th style="text-align: left">Unit Price</th>
                      <th style="text-align: left">Total</th>
                    </tr>
                  </thead>
                  <tbody> 
                    <?php                           
                      $number = 1;
                      $numElementsPerPage = 15; // How many elements per page
                      $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                      $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                      $rowCount = 0;
                      $sumPrice = 0;
                    ?>
                    @foreach($alldata as $data)
                      <?php 
                        $rowCount++;
                        $sumPrice += $data->quantity*$data->unit_price;
                      ?>
                    <tr>
                      <td>{{$currentNumber++}}</td>
                      <td>{{$data->productpurchase_product_object->name}}</td>
                      <td>{{$data->quantity}}</td>
                      <td>{{$data->unit_price}}</td>
                      <td>{{$data->quantity*$data->unit_price}}</td>
                    </tr>
                    @endforeach
                    @if($rowCount==0)
                      <tr>
                        <td colspan="5" align="center">
                          <h4 style="color: #ccc">No Data Found . . .</h4>
                        </td>
                      </tr>
                    @endif
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="4" style="text-align: right; border-top: 1px solid gray"><b>Sub Total</b></td>
                      <td style="border-top: 1px solid gray"><b>{{$sumPrice}}</b></td>
                    </tr>
                    <tr>
                      <td colspan="4" style="text-align: right"><b>(-) Discount</b></td>
                      <td><b>{{$singledata->discount}}</b></td>
                    </tr>
                    <tr>
                      <td colspan="4" style="text-align: right"><b>Grand Total</b></td>
                      <td><b>{{$grandTotal = $sumPrice-$singledata->discount}}</b></td>
                    </tr>
                    <tr>
                      <td colspan="4" style="text-align: right"><b>Paid Amount</b></td>
                      <td><b>{{$singledata->paid_amount}}</b></td>
                    </tr>
                    <tr>
                      <td colspan="4" style="text-align: right"><b>Due Amount</b></td>
                      <td><b>{{$grandTotal-$singledata->paid_amount}}</b></td>
                    </tr>
                  </tfoot>
                </table>
                <div class="col-md-12" align="right"></div>
              </div>
            </div>
            <div class="col-md-12"> 
              <!-- <input type="button" value="Print" onclick="PrintElem('#mydiv')" class="btn-success" /> -->
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer"></div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<script type="text/javascript">
  <?php
    $siteInfo = DB::table('site_setting')->where('id', 1)->first();
    $header = '';
    if (!empty($siteInfo)) {
      $logo = '../public/storage/app/public/uploads/logo/'.$siteInfo->image;
  
      $header = '<td width="20%"><img src="'.$logo.'" width="70px" height="70px"></td><td width="80%"><center><h3 style="margin:0; padding:0; margin-bottom: 0px">'.$siteInfo->company_name.'</h3>'.$siteInfo->address.'<br> Mobile : '.$siteInfo->phone.', Email : '.$siteInfo->email.'</center></td>';
    }
  ?>

  function PrintElem(elem)
  {
    Popup($(elem).html());
  }

  function Popup(data) 
  {   
    var mywindow = window.open('', 'my div', 'height=1000,width=1000');
    var is_chrome = Boolean(mywindow.chrome);
    mywindow.document.write('<html><head><title>Binary IT</title><style>a {text-decoration:none;}</style>');

    var Header = '<?php echo $header;?>';

    mywindow.document.write('<table width="80%" style="margin-left: auto; margin-right: auto"><tr>'+Header+'</tr></table><br>');
    
    mywindow.document.write('</head><body>');
    mywindow.document.write('<center><u><span style="font-weight: bold; font-size: 15px">Purchase Detail Report</span></u><center>');    
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
      
    if (is_chrome) {
      setTimeout(function() { // wait until all resources loaded 
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10
      mywindow.print(); // change window to winPrint
      mywindow.close(); // change window to winPrint
      }, 500);
    } else {
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10

      mywindow.print();
      mywindow.close();
    }
    return true;
  }
</script>
@endsection 