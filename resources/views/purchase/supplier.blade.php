@extends('layouts.layout')
@section('title', 'Manage Supplier')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Supplier</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Supplier</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Supplier List</h3>
                  <div>                   
                    <a href="javascript:void(0)" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
                      Add
                    </a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <form method="get" action="{{ route('customers.search') }}">
                    <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                    <div class="form-inline">
                      <div class="form-group mb-2">
                        <label for="inputPassword2" class="sr-only">Supplier Name</label>
                        <input type="text" class="form-control" name="name" placeholder="Supplier Name">
                      </div>&nbsp;&nbsp;
                      <button type="submit" class="btn btn-primary mb-2">Search</button>
                    </div>
                  </form>

                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>SL</th>
                          <th>Supplier ID</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Address</th>
                          <th width="20%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php                           
                          $number = 1;
                          $numElementsPerPage = 15; // How many elements per page
                          $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                          $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                          $rowCount = 0;
                        ?>
                        @foreach($alldata as $data)
                        <?php $rowCount++; ?>
                        <tr>
                          <td>
                          <label class="label label-success">{{$currentNumber++}}</label>
                        </td>
                        <td>{{$data->supplier_id}}</td>
                        <td><a href="{{$baseUrl.'/'.config('app.supplier').'/supplier-ledger/'.$data->id}}" target="_blank">{{$data->name}}</a></td>
                        <td>{{$data->email}}</td>
                        <td>{{$data->phone}}</td>
                        <td>{{$data->address}}</td>
                        <td>
                          <div class="form-inline">
                            <div class = "input-group">
                              <a href="#editModal{{$data->id}}" data-toggle="modal" class="btn btn-primary btn-xs" style="padding: 1px 15px"><i class="fa fa-edit"></i></a>     
                            </div>&nbsp;&nbsp;
                            <div class = "input-group">
                              <a href="#myModal_{{$data->id}}" data-toggle="modal" class="btn btn-primary btn-xs" style="padding: 1px 15px">Payment</a>     
                            </div>&nbsp;&nbsp;
                            <div class = "input-group"> 
                              {{Form::open(array('route'=>['product-supplier.destroy',$data->id],'method'=>'DELETE'))}}
                                <button type="submit" onclick="return confirm('Are you sure you want to delete ?')" class="btn btn-danger btn-xs" title="Delete" style="padding: 1px 9px;"><i class="fa fa-trash"></i></button>
                              {!! Form::close() !!}
                            </div>
                          </div>

                          <!-- Start Modal for edit Supplier -->
                            <div id="editModal{{$data->id}}" class="modal fade" role="dialog">
                              <div class="modal-dialog modal-md">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title"><i class="fa fa-edit"></i> EDIT SUPPLIER</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>

                                  {!! Form::open(array('route' =>['product-supplier.update', $data->id],'method'=>'PUT','files'=>true)) !!}
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label>Supplier Name</label>
                                          <strong style="color: red">*</strong>
                                          <input type="text" class="form-control" value="{{$data->name}}" name="name" autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                          <label>Email <span style="color:red">*</span></label>
                                          <input type="email" name="email" class="form-control" value="{{$data->email}}" autocomplete="off" required>
                                        </div>
                                        <div class="form-group">
                                          <label>Phone</label>
                                          <input type="text" name="phone" class="form-control" value="{{$data->phone}}" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                          <label>Address </label>
                                          <textarea name="address" class="form-control" autocomplete="off">{{$data->address}}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                    {{Form::submit('Update',array('class'=>'btn btn-success btn-sm', 'style'=>'width:15%'))}}
                                  </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                            </div>
                          <!-- End Modal for edit Supplier -->

                          <!-- Modal -->
                          <div id="myModal_{{$data->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                              {!! Form::open(array('route' =>['product-supplier.store'],'method'=>'POST')) !!}
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">Supplier Payment</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                  <div class="form-group"> 
                                    <label>Supplier Name</label>
                                    <input type="text" class="form-control" value="{{$data->name}}" readonly="">
                                  </div>
                                  <div class="form-group"> 
                                    <label>Due Amount</label>
                                    <?php
                                      $billAmount = DB::table('supplier_ledgers')->where('supplier_id', $data->id)->where('reason', 'like', '%' . 'purchase' . '%')->sum('amount');

                                      $payAmount = DB::table('supplier_ledgers')->where('supplier_id', $data->id)->where('reason', 'like', '%' . 'payment' . '%')->sum('amount');
                                      $dueAmount = $billAmount - $payAmount;
                                    ?>
                                    <input type="text" class="form-control" value="{{$dueAmount}}" readonly="">
                                  </div>
                                  <div class="form-group"> 
                                    <label>Amount</label>
                                    <input type="number" class="form-control" value="" name="pay_amount">
                                    <input type="hidden" value="{{$data->id}}" name="supplier_id">
                                  </div>
                                  <div class="form-group"> 
                                    <label>Payment Method</label>
                                    <select class="form-control" name="payment_method">
                                      @foreach($allbank as $bank)
                                      <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                  <div class="form-group"> 
                                    <label>Note</label>
                                    <textarea class="form-control" name="note" rows="1"></textarea>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                  <input type="submit" name="supplier_payment" class="btn btn-success btn-sm" value="Payment">
                                </div>
                              </div>
                              {!! Form::close() !!}
                            </div>
                          </div>
                          <!-- ./Modal -->
                        </td>
                        </tr>
                        @endforeach
                        @if($rowCount==0)
                        <tr>
                          <td colspan="7" align="center">
                            <h4 style="color: #ccc">No Data Found . . .</h4>
                          </td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  {{$alldata->withQueryString()->links()}}
                </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
<!-- /.content -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">ADD SUPPLIER</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(array('route' =>['product-supplier.store'],'method'=>'POST','files'=>true)) !!}
      <div class="modal-body">
        <div class="form-group">
          <?php
            $cusID = DB::table('suppliers')->orderBy('id', 'desc')->first();
            if (!empty($cusID->supplier_id)) {
              $id = $cusID->supplier_id+1;
            }else{
              $id = '1000';
            }
          ?>
          <label>Supplier ID <span style="color:red">*</span></label>
          <input type="text" name="supplier_id" class="form-control" value="<?php echo $id;?>" autocomplete="off" required="" readonly="">
        </div>
        <div class="form-group">
          <label>Name <span style="color:red">*</span></label>
          <input type="text" name="name" class="form-control" value="" autocomplete="off" required>
        </div>
        <div class="form-group">
          <label>Email <span style="color:red">*</span></label>
          <input type="email" name="email" class="form-control" value="" autocomplete="off" required>
        </div>
        <div class="form-group">
          <label>Phone</label>
          <input type="text" name="phone" class="form-control" value="" autocomplete="off">
        </div>
        <div class="form-group">
          <label>Address </label>
          <textarea name="address" class="form-control" autocomplete="off"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="save" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection