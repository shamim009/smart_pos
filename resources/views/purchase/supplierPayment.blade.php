@extends('layouts.layout')
@section('title', 'Supplier Payment')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
<section class="content-header">
  <h1> {{ __('messages.supplier_payment') }} <small></small> </h1>
  <ol class="breadcrumb">
    <li><a href="{{URL::To('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Payment</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  @include('common.message')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"> <i class="fa fa-list-alt"></i> {{ __('messages.supplier') }} {{ __('messages.list') }}</h3>
          <div class="form-inline pull-right">
            <div class="input-group">
              <a href="{{$baseUrl.'/'.config('app.supplier').'/product-supplier-payment-report'}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-list-alt"></i> Payment Report</a>
            </div>
            <div class="input-group">
              <a href="{{$baseUrl.'/'.config('app.account').'/bank-account'}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-list-alt"></i> Accounts</a>
            </div>
            <div class="input-group">
              <a href="{{$baseUrl.'/'.config('app.account').'/daily-transaction'}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-list-alt"></i> Daily Transaction</a>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-responsive table-hover myTable"> 
                  <thead> 
                    <tr> 
                      <th>{{ __('messages.SL') }}</th>
                      <th>{{ __('messages.id') }}</th>
                      <th>{{ __('messages.name') }}</th>
                      <th>{{ __('messages.email') }}</th>
                      <th>{{ __('messages.phone') }}</th>
                      <th>{{ __('messages.address') }}</th>
                      <th>{{ __('messages.area') }}</th>
                      <th>{{ __('messages.due') }}</th>
                      <th width="15%">{{ __('messages.action') }}</th>
                    </tr>
                  </thead>
                  <tbody> 
                    <?php                           
                      $number = 1;
                      $numElementsPerPage = 15; // How many elements per page
                      $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                      $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                      $rowCount = 0;
                    ?>
                    @foreach($alldata as $data)
                      <?php $rowCount++; ?>
                    <tr> 
                      <td>
                        <label class="label label-success">{{$currentNumber++}}</label>
                      </td>
                      <td>{{$data->supplier_id}}</td>
                      <td><a href="{{$baseUrl.'/'.config('app.supplier').'/supplier-ledger/'.$data->id}}" target="_blank">{{$data->name}}</a></td>
                      <td>{{$data->email}}</td>
                      <td>{{$data->phone}}</td>
                      <td>{{$data->address}}</td>
                      <td> 
                        @if(!empty($data->area_id))
                          {{$data->supplier_area_object->name}}
                        @endif
                      </td>
                      <td>
                        <?php
                          $billAmount = DB::table('supplier_ledger')->where('supplier_id', $data->id)->where('reason', 'like', '%' . 'purchase' . '%')->sum('amount');

                          $payAmount = DB::table('supplier_ledger')->where('supplier_id', $data->id)->where('reason', 'like', '%' . 'payment' . '%')->sum('amount');
                          echo $dueAmount = $billAmount - $payAmount;
                        ?>
                      </td>
                      <td>
                        <a class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal_{{$data->id}}">Payment</a>
                        <!-- Modal -->
                        <div id="myModal_{{$data->id}}" class="modal fade" role="dialog">
                          <div class="modal-dialog">
                            {!! Form::open(array('route' =>['product-supplier.store'],'method'=>'POST')) !!}
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">{{ __('messages.supplier_payment') }}</h4>
                              </div>
                              <div class="modal-body">
                                <div class="form-group"> 
                                  <label>{{ __('messages.supplier') }}</label>
                                  <input type="text" class="form-control" value="{{$data->name}}" readonly="">
                                </div>
                                <div class="form-group"> 
                                  <label>{{ __('messages.due_amount') }}</label>
                                  <?php
                                    $billAmount = DB::table('supplier_ledger')->where('supplier_id', $data->id)->where('reason', 'like', '%' . 'purchase' . '%')->sum('amount');

                                    $payAmount = DB::table('supplier_ledger')->where('supplier_id', $data->id)->where('reason', 'like', '%' . 'payment' . '%')->sum('amount');
                                    $dueAmount = $billAmount - $payAmount;
                                  ?>
                                  <input type="text" class="form-control" value="{{$dueAmount}}" readonly="">
                                </div>
                                <div class="form-group"> 
                                  <label>{{ __('messages.amount') }}</label>
                                  <input type="number" class="form-control" value="" name="pay_amount">
                                  <input type="hidden" value="{{$data->id}}" name="supplier_id">
                                </div>
                                <div class="form-group"> 
                                  <label>{{ __('messages.payment_method') }}</label>
                                  <select class="form-control" name="payment_method">
                                    @foreach($allbank as $bank)
                                    <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="form-group"> 
                                  <label>{{ __('messages.note') }}</label>
                                  <textarea class="form-control" name="note" rows="1"></textarea>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                <input type="submit" name="supplier_payment" class="btn btn-success btn-sm" value="Payment">
                              </div>
                            </div>
                            {!! Form::close() !!}
                          </div>
                        </div>
                        <!-- ./Modal -->
                      </td>
                    </tr>
                    @endforeach
                    @if($rowCount==0)
                      <tr>
                        <td colspan="9" align="center">
                          <h4 style="color: #ccc">No Data Found . . .</h4>
                        </td>
                      </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-md-12" align="right">
                  {{ $alldata->render() }}
                </div>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer"></div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
@endsection 