@extends('layouts.layout')
@section('title', 'Purchase Report')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
<section class="content-header">
  <h1> {{ __('messages.purchase_report') }} <small></small> </h1>
  <ol class="breadcrumb">
    <li><a href="{{URL::To('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Report</li>
  </ol>
</section>
<style type="text/css">
  .borderless td, .borderless th {
    border: none!important;
  }
</style>
<!-- Main content -->
<section class="content">
  @include('common.message')
  @include('common.commonFunction')
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"> <i class="fa fa-list-alt"></i> {{ __('messages.purchase_report') }}</h3>
          <div class="form-inline pull-right">
            <div class="input-group">
              <a href="{{$baseUrl.'/'.config('app.supplier').'/product-supplier'}}" class="btn btn-success btn-xs"><i class="fa fa-list-alt"></i> Supplier</a>
            </div>
            <div class="input-group">
              <a href="{{$baseUrl.'/'.config('app.purchase').'/product-purchase'}}" class="btn btn-success btn-xs"><i class="fa fa-get-pocket"></i> Product Purchase</a>
            </div>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12"> 
              <div class="table-responsive">
                <table class="table borderless">
                  <tbody>
                    <tr>
                      <td style="text-align: center;">
                        <form method="post" action="{{ route('purchase.filter') }}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-inline">
                            <div class="form-group">
                              <label>From : </label>
                              <input type="date" name="start_date" class="form-control" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="form-group">
                              <label>To : </label>
                              <input type="date" name="end_date" class="form-control" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="form-group">
                              <input type="submit" value="Search" class="btn btn-success btn-md">
                            </div>
                          </div>
                        </form>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
           
            <div class="col-md-12">
              @if(!empty($start_date) && !empty($end_date))
                <center><h4>From : {{dateFormateForView($start_date)}} To : {{dateFormateForView($end_date)}}</h4></center>
              @else
                <center><h4>Today : {{date('d-m-Y')}}</h4></center>
              @endif
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-responsive table-hover dataTableButton" width="100%"> 
                  <thead> 
                    <tr> 
                      <th style="text-align: left">{{ __('messages.SL') }}</th>
                      <th style="text-align: left">{{ __('messages.date') }}</th>
                      <th style="text-align: left">{{ __('messages.invoice') }}</th>
                      <th style="text-align: left">{{ __('messages.supplier') }}</th>
                      <th style="text-align: left">{{ __('messages.payment_method') }}</th>
                      <th style="text-align: left">{{ __('messages.created_by') }}</th>
                      <th style="text-align: left">{{ __('messages.row_total') }}</th>
                    </tr>
                  </thead>
                  <tbody> 
                    <?php                           
                      $number = 1;
                      $numElementsPerPage = 15; // How many elements per page
                      $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                      $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                      $rowCount = 0;

                      $totalPrice = 0;
                    ?>
                    @foreach($alldata as $data)
                      <?php 
                        $rowCount++;
                        $totalPrice += $data->sub_total;
                      ?>
                    <tr> 
                      <td>
                        {{$currentNumber++}}
                      </td>
                      <td>{{dateFormateForView($data->purchase_date)}}</td>
                      <td><a href="{{$baseUrl.'/'.config('app.purchase').'/purchase-report/'.$data->tok}}">{{$data->tok}}</a></td>
                      <td>{{$data->productpurchase_supplier_object->name}}</td>
                      <td>{{$data->productpurchase_paymentmethod_object->bank_name}}</td>
                      <td>{{$data->productpurchase_createdby_object->name}}</td>
                      <td>{{$data->sub_total}}</td>
                    </tr>
                    @endforeach
                    @if($rowCount==0)
                      <tr>
                        <td colspan="7" align="center">
                          <h4 style="color: #ccc">No Data Found . . .</h4>
                        </td>
                      </tr>
                    @endif
                  </tbody>
                  <tfoot> 
                    <tr> 
                      <td colspan="6" style="text-align: center"><b>{{ __('messages.total') }}</b></td>
                      <td><b>{{round($totalPrice, 2)}}</b></td>
                    </tr>
                  </tfoot>
                </table>
                <div class="col-md-12" align="right"></div>
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div>
        <div class="box-footer"></div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
@endsection 