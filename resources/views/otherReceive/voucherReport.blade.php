@extends('layouts.layout')
@section('title', 'Other Receive Voucher Report')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Other Receive Voucher Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
          @include('common.message')
          @include('common.commonFunction')
          </div>

          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Search Area</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="col-md-12">
                  <form method="post" action="{{ route('receive.filter') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-inline">
                      <div class="col-md-4" style="text-align: right;">
                        <div class="form-group">
                          <label>From : </label>
                          <input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d');?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>To : </label>
                          <input type="date" name="end_date" class="form-control" value="<?php echo date('Y-m-d');?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <input type="submit" value="Search" class="btn btn-success btn-md">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Receive Voucher Report</h3>
                  <div>                   
                    <a onclick="printReport();" href="javascript:0;"><img class="img-thumbnail" style="width:30px;" src='{{asset("custom/img/print.png")}}'></a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="col-md-12" id="printTable">
                  <center><h5 style="margin: 0px">Other Receive Voucher Report</h5></center>
                  @if(!empty($start_date) && !empty($end_date))
                    <center><h6 style="margin: 0px">From : {{dateFormateForView($start_date)}} To : {{dateFormateForView($end_date)}}</h6></center>
                  @else
                    <center><h6 style="margin: 0px">Today : {{date('d-m-Y')}}</h6></center>
                  @endif
                  <div class="table-responsive">
                    <table class="reportTable" style="width: 100%; font-size: 14px;" cellspacing="0" cellpadding="0"> 
                      <thead> 
                        <tr style="background: #ccc;"> 
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">SL</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Date</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Receive Method</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Type</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Sub Type</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Issue By</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Created By</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Amount</th>
                        </tr>
                      </thead>
                      <tbody> 
                        <?php                           
                          $number = 1;
                          $numElementsPerPage = 15; // How many elements per page
                          $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                          $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                          $rowCount = 0;

                          $sum = 0;
                        ?>
                        @foreach($alldata as $data)
                          <?php 
                            $rowCount++;
                            $sum += $data->amount;
                          ?>
                        <tr> 
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{$currentNumber++}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{dateFormateForView($data->receive_date)}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{$data->otherreceive_bank_object->bank_name}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{$data->otherreceive_type_object->name}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{$data->otherreceive_subtype_object->name}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{$data->issue_by}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{$data->otherreceive_user_object->name}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{number_format($data->amount, 2)}}</td>
                        </tr>
                        @endforeach
                        @if($rowCount==0)
                          <tr>
                            <td colspan="8" align="center">
                              <h4 style="color: #ccc">No Data Found . . .</h4>
                            </td>
                          </tr>
                        @endif
                      </tbody>
                      <tfoot> 
                        <tr> 
                          <td colspan="7" style="text-align: center;font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><b>Total</b></td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><b>{{number_format($sum, 2)}}</b></td>
                        </tr>
                      </tfoot>
                    </table>
                    <div class="col-md-12" align="right">{{$alldata->render()}}</div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection 