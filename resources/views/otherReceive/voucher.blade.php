@extends('layouts.layout')
@section('title', 'Add Other Receive Voucher')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Add Other Receive Voucher</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Voucher</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Other Receive Voucher</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(array('route' =>['receive-voucher.store'],'method'=>'POST')) !!}
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label>Receive Type</label>
                        <select class="form-control Type" name="receive_type_id" required=""> 
                          <option value="">Select</option>
                          @foreach($alltype as $type)
                          <option value="{{$type->id}}">{{$type->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group"> 
                        <label>Sub Type</label>
                        <select class="form-control SubType" name="receive_sub_type_id" required="">
                        </select>
                      </div>
                      <div class="form-group"> 
                        <label>Amount</label>
                        <input type="number" name="amount" class="form-control" value="" autocomplete="off" required>
                      </div>
                      <div class="form-group"> 
                        <label>Receive From</label>
                        <input type="text" name="receive_from" class="form-control" value="" autocomplete="off" required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label>Date</label>
                        <input type="text" name="receive_date" class="form-control datepicker" value="<?php echo date('Y-m-d');?>" required>
                      </div>
                      <div class="form-group"> 
                        <label>Receive Method</label>
                        <select class="form-control" name="bank_id" required="">
                          @foreach($allbank as $bank)
                          <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group"> 
                        <label>Issue By</label>
                        <input type="text" name="issue_by" class="form-control" autocomplete="off" value="">
                      </div>
                      <div class="form-group"> 
                        <label>Note</label>
                        <textarea name="note" rows="1" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              {!! Form::close() !!}
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
      // dependancy dropdown using ajax
      $(document).ready(function() {
        $('.Type').on('change', function() {
          var typeID = $(this).val();
          if(typeID) {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method: "POST",
              //url: "{{URL::to('find-receive-subtype-with-type-id')}}",
              url: "{{$baseUrl.'/'.config('app.or').'/find-receive-subtype-with-type-id'}}",
              data: {
                'id' : typeID
              },
              dataType: "json",

              success:function(data) {
                //console.log(data);
                if(data){
                  $('.SubType').empty();
                  $('.SubType').focus;
                  $('.SubType').append('<option value="">Select</option>'); 
                  $.each(data, function(key, value){
                    console.log(data);
                    $('select[name="receive_sub_type_id"]').append('<option value="'+ value.id +'">' + value.name+ '</option>');
                  });
                }else{
                  $('.SubType').empty();
                }
              }
            });
          }else{
            $('.SubType').empty();
          }
        });
      });
    </script>
@endsection 