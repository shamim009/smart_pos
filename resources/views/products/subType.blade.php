@extends('layouts.layout')
@section('title', 'Product Sub Type')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product Sub Type</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Sub Type</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Product Sub Type</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(array('route' =>['product-sub-type.store'],'method'=>'POST')) !!}
                <div class="card-body">
                  <div class="form-group"> 
                    <label>Type <span style="color:red">*</span></label>
                    <select class="form-control select2" name="product_type_id" required="">
                      <option value="">Select</option>
                      @foreach($alltype as $type)
                      <option value="{{$type->id}}">{{$type->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Name <span style="color:red">*</span></label>
                    <input type="text" name="name" class="form-control" value="" autocomplete="off" required>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              {!! Form::close() !!}
            </div>
            <!-- /.card -->
          </div>

          <!-- right column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Product Sub Type List</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Type</th>
                          <th>Name</th>
                          <th>Status</th>
                          <th style="width: 150px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php                           
                          $number = 1;
                          $numElementsPerPage = 15; // How many elements per page
                          $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                          $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                          $rowCount = 0;
                        ?>
                        @foreach($alldata as $data)
                        <?php $rowCount++; ?>
                        <tr>
                          <td>{{$currentNumber++}}</td>
                          <td>{{$data->productsubtype_type_object->name}}</td>
                          <td>{{$data->name}}</td>
                          <td>
                            @if ($data->status == 1)
                            <span class="badge bg-primary">Active</span>
                            @elseif ($data->status == 0)
                            <span class="badge bg-warning">Inactive</span>
                            @endif
                          </td>
                          <td>
                            <div class="form-inline">
                              <div class = "input-group">
                                <a href="#editModal{{$data->id}}" data-toggle="modal" class="btn btn-primary btn-xs" style="padding: 1px 15px"><i class="fa fa-edit"></i></a>     
                              </div>&nbsp;&nbsp;
                              <div class = "input-group"> 
                                {{Form::open(array('route'=>['product-sub-type.destroy',$data->id],'method'=>'DELETE'))}}
                                  <button type="submit" onclick="return confirm('Are you sure you want to delete ?')" class="btn btn-danger btn-xs" title="Delete" style="padding: 1px 9px;"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                              </div>
                            </div>

                            <!-- Start Modal for edit Class -->
                            <div id="editModal{{$data->id}}" class="modal fade" role="dialog">
                              <div class="modal-dialog modal-md">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Product Sub Type</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>

                                  {!! Form::open(array('route' =>['product-sub-type.update', $data->id],'method'=>'PUT')) !!}
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label>Type <strong style="color: red">*</strong></label>
                                          <select class="form-control" name="product_type_id" required="">
                                            <option value="">Select</option>
                                            @foreach($alltype as $type)
                                            <option value="{{$type->id}}" {{($type->id==$data->product_type_id)? 'selected':''}}>{{$type->name}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group">
                                          <label for="Group Name">Name <strong style="color: red">*</strong></label>
                                          <input type="text" class="form-control" value="{{$data->name}}" name="name" autocomplete="off" required>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                    {{Form::submit('Update',array('class'=>'btn btn-success btn-sm', 'style'=>'width:15%'))}}
                                  </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                            </div>
                            <!-- End Modal for edit Class -->
                          </td>
                        </tr>
                        @endforeach
                        @if($rowCount==0)
                        <tr>
                          <td colspan="5" align="center">
                            <h4 style="color: #ccc">No Data Found . . .</h4>
                          </td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  {{$alldata->render()}}
                </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection 