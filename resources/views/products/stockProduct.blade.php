@extends('layouts.layout')
@section('title', 'Stock Product')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Stock Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Stock Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Stock Product</h3>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="text-align: left">SL</th>
                          <th style="text-align: left">Product Name</th>
                          <th style="text-align: left">Product Unit</th>
                          <th style="text-align: left">Quantity</th>
                          <th style="text-align: left">Unit Price</th>
                          <th style="text-align: left">Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php                           
                          $number = 1;
                          $numElementsPerPage = 15; // How many elements per page
                          $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                          $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                          $rowCount = 0;

                          $totalQnty = 0;
                          $totalPrice = 0;
                        ?>
                        @foreach($alldata as $data)
                        <?php 
                          $rowCount++; 
                          $totalQnty += $data->quantity;
                          $totalPrice += $data->quantity*$data->unit_price;
                        ?>
                        <tr>
                          <td>{{$currentNumber++}}</td>
                          <td>{{$data->stockproduct_product_object->name}}</td>
                          <td> 
                            <?php
                               $unit_name = DB::table('product')
                                ->select('product_unit.name')
                                ->join('product_unit','product.unit_id','=','product_unit.id')
                                ->where('product.id', $data->product_id)
                                ->first();
                                echo $unit_name->name;
                            ?>
                          </td>
                          <td>{{$data->quantity}}</td>
                          <td>{{$data->unit_price}}</td>
                          <td>{{round($data->quantity*$data->unit_price, 2)}}</td>
                        </tr>
                        @endforeach
                        @if($rowCount==0)
                        <tr>
                          <td colspan="6" align="center">
                            <h4 style="color: #ccc">No Data Found . . .</h4>
                          </td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  {{$alldata->render()}}
                </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection