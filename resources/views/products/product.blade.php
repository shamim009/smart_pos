@extends('layouts.layout')
@section('title', 'Product')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Brand</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Add Product</h3>
                  <div>                   
                    <a href="javascript:void(0)" class="btn btn-sm btn-info" data-toggle="modal" data-target="#exampleModal">
                      Add
                    </a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>SL</th>
                          <th>Name</th>
                          <th>Type</th>
                          <th>Sub Type</th>
                          <th>Brand</th>
                          <th>Unit</th>
                          <th>Sell Price</th>
                          <th style="width: 150px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php                           
                          $number = 1;
                          $numElementsPerPage = 15; // How many elements per page
                          $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                          $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                          $rowCount = 0;
                        ?>
                        @foreach($alldata as $data)
                        <?php $rowCount++; ?>
                        <tr>
                        <td>
                        <label class="label label-success">{{$currentNumber++}}</label>
                        </td>
                        <td>{{Str::limit($data->name, 10)}}</td>
                        <td>{{Str::limit($data->product_type_object->name, 10)}}</td>
                        <td>
                          @if(!empty($data->product_sub_type_id))
                          {{Str::limit($data->product_subtype_object->name, 10)}}
                          @endif
                        </td>
                        <td> 
                          @if(!empty($data->brand_id))
                          {{Str::limit($data->product_brand_object->name, 10)}}
                          @endif
                        </td>
                        <td> 
                          @if(!empty($data->unit_id))
                          {{$data->product_unit_object->name}}
                          @endif
                        </td>
                        <td>{{$data->price}}</td>
                        <td>
                          <div class="form-inline">
                            <div class = "input-group">
                              <!--a href="#barCode{{$data->id}}" data-toggle="modal" class="btn btn-warning btn-xs" style="padding: 1px 7px"><i class="fa fa-barcode"></i></a-->     
                              <a href="{{url('barcode', $data->bar_code)}}" class="btn btn-warning btn-xs" style="padding: 1px 7px"><i class="fa fa-barcode"></i></a>     
                            </div>&nbsp;&nbsp;
                            <div class = "input-group">
                              <a href="#editModal{{$data->id}}" data-toggle="modal" class="btn btn-primary btn-xs" style="padding: 1px 9px"><i class="fa fa-edit"></i></a>     
                            </div>&nbsp;&nbsp;
                            <div class = "input-group"> 
                              {{Form::open(array('route'=>['product.destroy',$data->id],'method'=>'DELETE'))}}
                                <button type="submit" onclick="return confirm('Are you sure you want to delete ?')" class="btn btn-danger btn-xs" title="Delete" style="padding: 1px 9px;"><i class="fa fa-trash"></i></button>
                              {!! Form::close() !!}
                            </div>
                          </div>

                          <!-- Start Modal for edit type -->
                          <div id="editModal{{$data->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-md">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Product</h4>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                {!! Form::open(array('route' =>['product.update', $data->id],'method'=>'PUT')) !!}
                                <div class="modal-body">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label for="Group Name">Name</label>
                                        <strong style="color: red">*</strong> :
                                        <input type="text" class="form-control" value="{{$data->name}}" name="name" autocomplete="off" required>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="Group Name">Type</label>
                                        <select class="form-control Type" name="product_type_id"> 
                                          <option value="">Select</option>
                                          @foreach($alltype as $type)
                                          <option value="{{$type->id}}" <?php echo($type->id==$data->product_type_id)? 'selected':''?>>{{$type->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="Group Name">Sub Type</label>
                                        <select class="form-control SubType" name="product_sub_type_id"> 
                                          <option value="">Select</option>
                                          @foreach($allsubtype as $subtype)
                                          <option value="{{$subtype->id}}" <?php echo($subtype->id==$data->product_sub_type_id)? 'selected':''?>>{{$subtype->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="Group Name">Brand</label>
                                        <select class="form-control" name="brand_id"> 
                                          <option value="">Select</option>
                                          @foreach($allbrand as $brand)
                                          <option value="{{$brand->id}}" <?php echo($brand->id==$data->brand_id)? 'selected':''?>>{{$brand->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="Group Name">Unit</label>
                                        <select class="form-control" name="unit_id"> 
                                          <option value="">Select</option>
                                          @foreach($allunit as $unit)
                                          <option value="{{$unit->id}}" <?php echo($unit->id==$data->unit_id)? 'selected':''?>>{{$unit->name}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group"> 
                                        <label>Sell Price</label>
                                        <input type="text" name="price" class="form-control" value="{{$data->price}}" autocomplete="off">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group"> 
                                        <label>Vat (%)</label>
                                        <input type="text" name="vat_percent" class="form-control" value="{{$data->vat_percent}}" autocomplete="off">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                  {{Form::submit('Update',array('class'=>'btn btn-success btn-sm', 'style'=>'width:15%'))}}
                                </div>
                                {!! Form::close() !!}
                              </div>
                            </div>
                          </div>
                          <!-- End Modal for edit type -->
                        </td>
                        </tr>
                        @endforeach
                        @if($rowCount==0)
                        <tr>
                          <td colspan="8" align="center">
                            <h4 style="color: #ccc">No Data Found . . .</h4>
                          </td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  {{$alldata->render()}}
                </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">ADD PRODUCT</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {!! Form::open(array('route' =>['product.store'],'method'=>'POST')) !!}
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group"> 
                  <label>Bar Code <span style="color: red">*</span></label>
                  <input type="text" name="bar_code" class="form-control" value="" autocomplete="off" required>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group"> 
                  <label>Name <span style="color: red">*</span></label>
                  <input type="text" name="name" class="form-control" value="" autocomplete="off" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>Type <span style="color: red">*</span></label>
                  <select class="form-control Type select2" name="product_type_id" required=""> 
                    <option value="">Select</option>
                    @foreach($alltype as $type)
                    <option value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>Sub Type</label>
                  <select class="form-control SubType select2" name="product_sub_type_id">
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>Product Brand</label>
                  <select class="form-control select2" name="brand_id"> 
                    <option value="">Select</option>
                    @foreach($allbrand as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>Product Unit <span style="color: red">*</span></label>
                  <select class="form-control select2" name="unit_id" required=""> 
                    <option value="">Select</option>
                    @foreach($allunit as $unit)
                    <option value="{{$unit->id}}">{{$unit->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>Sell Price</label>
                  <input type="number" name="price" class="form-control" value="" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label>Vat (%)</label>
                  <input type="number" name="vat_percent" class="form-control" value="0" min="0" autocomplete="off">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="save" class="btn btn-primary">Save</button>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
      // dependancy dropdown using ajax
      $(document).ready(function() {
        $('.Type').on('change', function() {
          var typeID = $(this).val();
          if(typeID) {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method: "POST",
              //url: "{{URL::to('find-product-subtype-with-type-id')}}",
              url: "{{$baseUrl.'/'.config('app.product').'/find-product-subtype-with-type-id'}}",
              data: {
                'id' : typeID
              },
              dataType: "json",

              success:function(data) {
                //console.log(data);
                if(data){
                  $('.SubType').empty();
                  $('.SubType').focus;
                  $('.SubType').append('<option value="">Select</option>'); 
                  $.each(data, function(key, value){
                    //console.log(data);
                    $('select[name="product_sub_type_id"]').append('<option value="'+ value.id +'">' + value.name+ '</option>');
                  });
                }else{
                  $('.SubType').empty();
                }
              }
            });
          }else{
            $('.SubType').empty();
          }
        });
      });
    </script>
@endsection