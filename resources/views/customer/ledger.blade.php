@extends('layouts.layout')
@section('title', 'Customer Ledger')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Customer Ledger</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Ledger</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
        
          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Customer Information</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <td>Name</td>
                          <td>{{$customer_info->name}}</td>
                          <td rowspan="5" width="20%">
                            <img src="/storage/app/public/uploads/customer_image/{{$customer_info->image}}" width="100%" height="200px">
                          </td>
                        </tr>
                        <tr>
                          <td>Phone</td>
                          <td>{{$customer_info->phone}}</td>
                        </tr>
                        <tr>
                          <td>NID No</td>
                          <td>{{$customer_info->nid_no}}</td>
                        </tr>
                        <tr>
                          <td>Company</td>
                          <td>{{$customer_info->company}}</td>
                        </tr>
                        <tr>
                          <td>Address</td>
                          <td>{{$customer_info->address}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>              
            </div>
            <!-- /.card -->
          </div>

          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <td>#</td>
                          <td>Date</td>
                          <td>Reason</td>
                          <td>Amount</td>
                          <td>Balance</td>
                        </tr>
                      </thead>
                      <tbody>
                          <?php                           
                            $number = 1;
                            $numElementsPerPage = 15; // How many elements per page
                            $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                            $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                            $rowCount = 0;
                            $sum = 0;
                          ?>
                          @foreach($alldata as $data)
                          <?php $rowCount++; $sum += $data->amount;?>
                          <tr>
                            <td>{{$currentNumber++}}</td>
                            <td>{{date('d-m-Y', strtotime($data->date))}}</td>
                            <td>{{$data->reason}}</td>
                            <td>{{number_format($data->amount, 2)}}</td>
                            <td>{{number_format($sum, 2)}}</td>
                          </tr>
                          @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4"><center>Paid</center></td>
                          <td>{{number_format($sum, 2)}}</td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
<!-- /.content -->
@endsection 