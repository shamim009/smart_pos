<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  {!!Html::style('custom/plugins/fontawesome-free/css/all.min.css')!!}
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  {!!Html::style('custom/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')!!}
  <!-- iCheck -->
  {!!Html::style('custom/plugins/icheck-bootstrap/icheck-bootstrap.min.css')!!}
  <!-- JQVMap -->
  {!!Html::style('custom/plugins/jqvmap/jqvmap.min.css')!!}
  <!-- Theme style -->
  {!!Html::style('custom/dist/css/adminlte.min.css')!!}
  <!-- overlayScrollbars -->
  {!!Html::style('custom/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')!!}
  <!-- Daterange picker -->
  {!!Html::style('custom/plugins/daterangepicker/daterangepicker.css')!!}
  <!-- summernote -->
  {!!Html::style('custom/plugins/summernote/summernote-bs4.min.css')!!}
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="{{asset('custom/dist/img/AdminLTELogo.png')}}" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-comments"></i>
          <span class="badge badge-danger navbar-badge">3</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('custom/dist/img/user1-128x128.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Brad Diesel
                  <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">Call me whenever you can...</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('custom/dist/img/user8-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  John Pierce
                  <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">I got your message bro</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('custom/dist/img/user3-128x128.jpg')}}" alt="User Avatar" class="img-size-50 img-circle mr-3">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Nora Silvester
                  <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                </h3>
                <p class="text-sm">The subject goes here</p>
                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="{{ route('logout') }}" role="button" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> Sign Out
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{URL::To('/home')}}" class="brand-link">
      <img src="{{asset('custom/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">CPanel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('custom/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>
      <?php
        $baseUrl = URL::to('/');
        $url = Request::path();
      ?>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item {{($url=='home') ? 'active':''}}">
            <a href="{{URL::To('/home')}}" class="nav-link {{($url=='home') ? 'active':''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item {{($url==config('app.account').'/account-type' || $url==config('app.account').'/bank-account' || $url==config('app.account').'/cheque-book' || $url==config('app.account').'/cheque-no' || $url==config('app.account').'/final-report' || $url==(request()->is(config('app.account').'/bank-deposit/*')) || $url==(request()->is(config('app.account').'/amount-withdraw/*')) || $url==(request()->is(config('app.account').'/amount-transfer/*')) || $url==(request()->is(config('app.account').'/bank-report/*'))) ? 'menu-is-opening menu-open':''}}">
            <a href="#" class="nav-link {{($url==config('app.account').'/account-type' || $url==config('app.account').'/bank-account' || $url==config('app.account').'/cheque-book' || $url==config('app.account').'/cheque-no' || $url==config('app.account').'/final-report' || $url==(request()->is(config('app.account').'/bank-deposit/*')) || $url==(request()->is(config('app.account').'/amount-withdraw/*')) || $url==(request()->is(config('app.account').'/amount-transfer/*')) || $url==(request()->is(config('app.account').'/bank-report/*'))) ? 'active':''}}">
              <i class="nav-icon fas fa-hand-holding-usd"></i>
              <p>
                Account
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.account').'/account-type'}}" class="nav-link {{($url==config('app.account').'/account-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Account Type</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.account').'/bank-account'}}" class="nav-link {{($url==config('app.account').'/bank-account' || $url==(request()->is(config('app.account').'/bank-deposit/*')) || $url==(request()->is(config('app.account').'/amount-withdraw/*')) || $url==(request()->is(config('app.account').'/amount-transfer/*')) || $url==(request()->is(config('app.account').'/bank-report/*'))) ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Bank Account</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.account').'/cheque-book'}}" class="nav-link {{($url==config('app.account').'/cheque-book') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cheque Book</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.account').'/cheque-no'}}" class="nav-link {{($url==config('app.account').'/cheque-no') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cheque No</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.account').'/final-report'}}" class="nav-link {{($url==config('app.account').'/final-report') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Final Balance</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item {{($url==config('app.or').'/receive-type' || $url==config('app.or').'/receive-sub-type' || $url==config('app.or').'/receive-voucher' || $url==config('app.or').'/receive-voucher-report') ? 'menu-is-opening menu-open':''}}">
            <a href="#" class="nav-link {{($url==config('app.or').'/receive-type' || $url==config('app.or').'/receive-sub-type' || $url==config('app.or').'/receive-voucher' || $url==config('app.or').'/receive-voucher-report') ? 'active':''}}">
              <i class="nav-icon fas fa-receipt"></i>
              <p>
                Other Receive
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.or').'/receive-type'}}" class="nav-link {{($url==config('app.or').'/receive-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Receive Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.or').'/receive-sub-type'}}" class="nav-link {{($url==config('app.or').'/receive-sub-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Receive Sub Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.or').'/receive-voucher'}}" class="nav-link {{($url==config('app.or').'/receive-voucher') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Receive Voucher</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.or').'/receive-voucher-report'}}" class="nav-link {{($url==config('app.or').'/receive-voucher-report') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Voucher Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item {{($url==config('app.op').'/payment-type' || $url==config('app.op').'/payment-sub-type' || $url==config('app.op').'/payment-voucher' || $url==config('app.op').'/payment-voucher-report') ? 'menu-is-opening menu-open':''}}">
            <a href="#" class="nav-link {{($url==config('app.op').'/payment-type' || $url==config('app.op').'/payment-sub-type' || $url==config('app.op').'/payment-voucher' || $url==config('app.op').'/payment-voucher-report') ? 'active':''}}">
              <i class="nav-icon fas fa-receipt"></i>
              <p>
                Other Payment
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.op').'/payment-type'}}" class="nav-link {{($url==config('app.op').'/payment-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.op').'/payment-sub-type'}}" class="nav-link {{($url==config('app.op').'/payment-sub-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Sub Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.op').'/payment-voucher'}}" class="nav-link {{($url==config('app.op').'/payment-voucher') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Voucher</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.op').'/payment-voucher-report'}}" class="nav-link {{($url==config('app.op').'/payment-voucher-report') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item {{($url==config('app.customer').'/customer') ? 'menu-is-opening menu-open':''}}">
            <a href="#" class="nav-link {{($url==config('app.customer').'/customer') ? 'active':''}}">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Manage Customer
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.customer').'/customer'}}" class="nav-link {{($url==config('app.customer').'/customer') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Customer</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                HR Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Demo</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item {{($url==config('app.product').'/product-type' || $url==config('app.product').'/product' || $url==config('app.product').'/product-brand' || $url==config('app.product').'/stock-product' || $url==config('app.product').'/product-sub-type' || $url==config('app.product').'/product-unit' || $url==config('app.product').'/update-stock-product') ? 'menu-is-opening menu-open':''}}">
            <a href="#" class="nav-link {{($url==config('app.product').'/product-type' || $url==config('app.product').'/product' || $url==config('app.product').'/product-brand' || $url==config('app.product').'/stock-product' || $url==config('app.product').'/product-sub-type' || $url==config('app.product').'/product-unit' || $url==config('app.product').'/update-stock-product') ? 'active':''}}">
              <i class="nav-icon fab fa-product-hunt"></i>
              <p>
                Manage Product
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.product').'/product-type'}}" class="nav-link {{($url==config('app.product').'/product-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.product').'/product-sub-type'}}" class="nav-link {{($url==config('app.product').'/product-sub-type') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Sub Type</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.product').'/product-brand'}}" class="nav-link {{($url==config('app.product').'/product-brand') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Brand</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.product').'/product-unit'}}" class="nav-link {{($url==config('app.product').'/product-unit') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Product Unit</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.product').'/product'}}" class="nav-link {{($url==config('app.product').'/product') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All Products</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.product').'/stock-product'}}" class="nav-link {{($url==config('app.product').'/stock-product') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Stock Products</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item {{($url==config('app.supplier').'/product-supplier' || $url==config('app.supplier').'/product-supplier-payment' || $url==config('app.supplier').'/product-supplier-payment-report') ? 'menu-is-opening menu-open':''}}">
            <a href="#" class="nav-link {{($url==config('app.supplier').'/product-supplier' || $url==config('app.supplier').'/product-supplier-payment' || $url==config('app.supplier').'/product-supplier-payment-report') ? 'active':''}}">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Manage Supplier
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.supplier').'/product-supplier'}}" class="nav-link {{($url==config('app.supplier').'/product-supplier') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.supplier').'/product-supplier-payment'}}" class="nav-link {{($url==config('app.supplier').'/product-supplier-payment') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Supplier Payment</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{$baseUrl.'/'.config('app.supplier').'/product-supplier-payment-report'}}" class="nav-link {{($url==config('app.supplier').'/product-supplier-payment-report') ? 'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payment Report</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-header">EXAMPLES</li>
          <li class="nav-item">
            <a href="pages/gallery.html" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.2.0-rc
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
{!!Html::script('custom/plugins/jquery/jquery.min.js')!!}
<!-- jQuery UI 1.11.4 -->
{!!Html::script('custom/plugins/jquery-ui/jquery-ui.min.js')!!}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
{!!Html::script('custom/plugins/bootstrap/js/bootstrap.bundle.min.js')!!}
<!-- ChartJS -->
{!!Html::script('custom/plugins/chart.js/Chart.min.js')!!}
<!-- Sparkline -->
{!!Html::script('custom/plugins/sparklines/sparkline.js')!!}
<!-- JQVMap -->
{!!Html::script('custom/plugins/jqvmap/jquery.vmap.min.js')!!}
{!!Html::script('custom/plugins/jqvmap/maps/jquery.vmap.usa.js')!!}
<!-- jQuery Knob Chart -->
{!!Html::script('custom/plugins/jquery-knob/jquery.knob.min.js')!!}
<!-- daterangepicker -->
{!!Html::script('custom/plugins/moment/moment.min.js')!!}
{!!Html::script('custom/plugins/daterangepicker/daterangepicker.js')!!}
<!-- Tempusdominus Bootstrap 4 -->
{!!Html::script('custom/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')!!}
<!-- Summernote -->
{!!Html::script('custom/plugins/summernote/summernote-bs4.min.js')!!}
<!-- overlayScrollbars -->
{!!Html::script('custom/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')!!}
<!-- AdminLTE App -->
{!!Html::script('custom/dist/js/adminlte.js')!!}
<!-- AdminLTE for demo purposes -->
{!!Html::script('custom/dist/js/demo.js')!!}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{!!Html::script('custom/dist/js/pages/dashboard.js')!!}
</body>
</html>