@extends('layouts.layout')
@section('title', 'Amount Withdraw')
@section('content')
<?php
  $baseUrl = URL::to('/');
?>
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Amount Withdraw</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Withdraw</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Amount Withdraw</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(array('route' =>['amount-withdraw.store'],'method'=>'POST')) !!}
                <div class="card-body">
                  <div class="form-group"> 
                    <label>Bank Name</label>
                    <input type="text" class="form-control" value="{{$alldata->bank_name}}" readonly="">
                    <input type="hidden" class="form-control" name="bank_id" value="{{$alldata->id}}">
                  </div>
                  <div class="form-group"> 
                    <label>Account Name</label>
                    <input type="text" class="form-control" value="{{$alldata->account_name}}" readonly="">
                  </div>
                  <div class="form-group"> 
                    <label>Account No</label>
                    <input type="text" class="form-control" value="{{$alldata->account_no}}" readonly="">
                  </div>
                  <div class="form-group"> 
                    <label>Cheque Book</label>
                    <select class="form-control Checkbook" name="check_book" required=""> 
                      <option value="">Selcct</option>
                      @foreach($allchequebook as $chequebook)
                      <option value="{{$chequebook->id}}">{{$chequebook->name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group"> 
                    <label>Cheque No</label>
                    <select class="form-control Checkno" name="check_no"> 
                      
                    </select>
                  </div>
                  <div class="form-group"> 
                    <label>Amount</label>
                    <input type="number" name="withdraw_amount" class="form-control">
                  </div>
                  <div class="form-group"> 
                    <label>Date</label>
                    <input type="text" name="transaction_date" class="form-control datepicker" value="{{date('Y-m-d')}}">
                  </div>
                  <div class="form-group"> 
                    <label>Note</label>
                    <textarea class="form-control" name="note"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              {!! Form::close() !!}
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript"> 
      // dependancy dropdown using ajax
      $(document).ready(function() {
        $('.Checkbook').on('change', function() {
          var chequeBookID = $(this).val();
          if(chequeBookID) {
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method: "POST",
              //url: "{{URL::to('find-chequeno-with-chequebook-id')}}",
              url: "{{$baseUrl.'/'.config('app.account').'/find-chequeno-with-chequebook-id'}}",
              data: {
                'id' : chequeBookID
              },
              dataType: "json",

              success:function(data) {
               // console.log(data);
                if(data){
                  $('.Checkno').empty();
                  $('.Checkno').focus;
                  $('.Checkno').append('<option value="">Select</option>');
                  $.each(data, function(key, value){
                    //console.log(data);
                    $('select[name="check_no"]').append('<option value="'+ value.id +'">' + value.cheque_no+ '</option>');
                  });
                }else{
                  $('.Checkno').empty();
                }
              }
            });
          }else{
            $('.Checkno').empty();
          }
        });
      });
    </script>
@endsection 