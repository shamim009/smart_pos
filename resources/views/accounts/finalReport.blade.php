@extends('layouts.layout')
@section('title', 'Final Report')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Final Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
          @include('common.message')
          @include('common.commonFunction')
          </div>

          <!-- right column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Search Area</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="col-md-12">
                  <form method="post" action="{{ route('final-report.filter') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-inline">
                      <div class="col-md-4" style="text-align: right;">
                        <div class="form-group">
                          <label>From : </label>
                          <input type="date" name="start_date" class="form-control" value="<?php echo date('Y-m-d');?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>To : </label>
                          <input type="date" name="end_date" class="form-control" value="<?php echo date('Y-m-d');?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <input type="submit" value="Search" class="btn btn-success btn-md">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Final Report</h3>
                  <div>                   
                    <a onclick="printReport();" href="javascript:0;"><img class="img-thumbnail" style="width:30px;" src='{{asset("custom/img/print.png")}}'></a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="col-md-12" id="printTable">
                  <center><h5 style="margin: 0px">Final Report</h5></center>
                  @if(!empty($start_date) && !empty($end_date))
                    <center><h6 style="margin: 0px">From : {{dateFormateForView($start_date)}} To : {{dateFormateForView($end_date)}}</h6></center>
                  @else
                    <center><h6 style="margin: 0px">Today : {{date('d-m-Y')}}</h6></center>
                  @endif
                  <div class="table-responsive">
                    <table class="reportTable" style="width: 100%; font-size: 14px;" cellspacing="0" cellpadding="0"> 
                      <thead> 
                        <tr style="background: #ccc;"> 
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">SL</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Particular</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Credit</th>
                          <th style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Debit</th>
                        </tr>
                      </thead>
                      <tbody> 
                        <?php 
                          $sum = 0;
                          $totalDebit = 0;
                          $totalCredit = 0;
                        ?>
                        <tr> 
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">1</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Total Other Payment</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">0.00</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{number_format($other_total_payment, 2)}}</td>
                        </tr>
                        <tr> 
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">2</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Total Other Receive</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{number_format($other_total_received, 2)}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">0.00</td>
                        </tr>
                        <tr> 
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">3</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">Total Bill Collection</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">{{number_format($bill_collection, 2)}}</td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px">0.00</td>
                        </tr>
                      </tbody>
                      <tfoot> 
                        <?php
                          $totalCredit = $other_total_received+$bill_collection;
                          $totalDebit = $other_total_payment;
                        ?>
                        <tr> 
                          <td colspan="2" style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><center><b>Total</b></center></td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><b><?php echo number_format($totalCredit, 2);?></b></td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><b><?php echo number_format($totalDebit, 2);?></b></td>
                        </tr>
                        <tr> 
                          <td colspan="2" style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><center><b>Final Balance</b></center></td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"><b><?php echo number_format(($totalCredit-$totalDebit), 2);?></b></td>
                          <td style="font-weight: bold; border: 1px solid #ddd; padding: 3px 3px"></td>
                        </tr>
                      </tfoot>
                    </table>
                    <div class="col-md-12" align="right"></div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection 