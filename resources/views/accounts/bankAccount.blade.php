@extends('layouts.layout')
@section('title', 'Bank Account')
@section('content')
<!-- Content Header (Page header) -->
<?php
  $baseUrl = URL::to('/');
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Bank Account</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Account</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Bank Account List</h3>
                  <div>                   
                    <a href="javascript:void(0)" class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-circle"></i> <b>ADD BANK</b></a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th style="width: 10px">#</th>
                          <th>Name</th>
                          <th>Account Name</th>
                          <th>Account No</th>
                          <th>Account Type</th>
                          <th>Branch</th>
                          <th>Balance</th>
                          <th>Status</th>
                          <th width="40px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php                           
                          $number = 1;
                          $numElementsPerPage = 15; // How many elements per page
                          $pageNumber = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                          $currentNumber = ($pageNumber - 1) * $numElementsPerPage + $number;
                          $rowCount = 0;
                        ?>
                        @foreach($alldata as $data)
                        <?php $rowCount++; ?>
                        <tr>
                          <td>{{$currentNumber++}}</td>
                          <td> 
                            <a href="{{$baseUrl.'/'.config('app.account').'/bank-report/'.$data->id}}">{{$data->bank_name}}</a>
                          </td>
                          <td>{{$data->account_name}}</td>
                          <td>{{$data->account_no}}</td>
                          <td> 
                            <span class="label label-success">{{$data->bankaccount_accounttype_object->name}}</span>
                          </td>
                          <td>{{$data->bank_branch}}</td>
                          <td>{{$data->balance}}</td>
                          <td>
                            @if ($data->status == 1)
                            <span class="badge bg-primary">Active</span>
                            @elseif ($data->status == 0)
                            <span class="badge bg-warning">Inactive</span>
                            @endif
                          </td>
                          <td>
                            <div class="btn-group">
                              <button type="button" class="btn btn-info btn-xs">Action</button>
                              <button type="button" class="btn btn-info btn-xs dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, -165px, 0px);">
                                <a class="dropdown-item" href="{{$baseUrl.'/'.config('app.account').'/bank-deposit/'.$data->id}}">Deposit</a>
                                <a class="dropdown-item" href="{{$baseUrl.'/'.config('app.account').'/amount-withdraw/'.$data->id}}">Withdraw</a>
                                <a class="dropdown-item" href="{{$baseUrl.'/'.config('app.account').'/amount-transfer/'.$data->id}}">Transfer</a>
                                <a class="dropdown-item" href="{{$baseUrl.'/'.config('app.account').'/bank-report/'.$data->id}}">Report</a>
                                <a class="dropdown-item" href="#editModal{{$data->id}}" data-toggle="modal">Edit</a>
                              </div>
                            </div>

                            <!-- Start Modal for edit bank -->
                            <div id="editModal{{$data->id}}" class="modal fade" role="dialog">
                              <div class="modal-dialog modal-md">
                                <!-- Modal content-->
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Bank Account</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  </div>

                                  {!! Form::open(array('route' =>['bank-account.update', $data->id],'method'=>'PUT')) !!}
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group"> 
                                          <label>Bank Name</label>
                                          <input type="text" name="bank_name" class="form-control" value="{{$data->bank_name}}" autocomplete="off" required>
                                        </div>
                                        <div class="form-group"> 
                                          <label>Account Name</label>
                                          <input type="text" name="account_name" class="form-control" value="{{$data->account_name}}" autocomplete="off" required>
                                        </div>
                                        <div class="form-group"> 
                                          <label>Account No</label>
                                          <input type="text" name="account_no" class="form-control" value="{{$data->account_no}}" autocomplete="off" required>
                                        </div>
                                        <div class="form-group"> 
                                          <label>Account Type</label>
                                          <select class="form-control" name="account_type"> 
                                            <option value="">Select</option>
                                            @foreach($allaccounttype as $type)
                                            <option value="{{$type->id}}" {{($type->id==$data->account_type)? 'selected':''}}>{{$type->name}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group"> 
                                          <label>Branch</label>
                                          <input type="text" name="bank_branch" class="form-control" value="{{$data->bank_branch}}" autocomplete="off" required>
                                        </div>
                                        <div class="form-group"> 
                                          <label>Opening Balance</label>
                                          <?php
                                            // getting bank opening balance
                                            $openingBalance = DB::table('transaction_reports')->where('bank_id', $data->id)->where('reason', 'LIKE', '%' . 'Opening Balance' . '%')->first();
                                          ?>
                                          <input type="hidden" name="bank_balance" class="form-control" value="{{$data->balance}}">
                                          <input type="hidden" name="old_opening_balance" class="form-control" value="{{$openingBalance->amount}}">
                                          <input type="number" name="new_opening_balance" class="form-control" value="{{$openingBalance->amount}}" required>
                                        </div>
                                        <div class="form-group"> 
                                          <label>Date</label>
                                          <input type="text" name="opening_date" class="form-control datepicker" value="{{$data->opening_date}}" autocomplete="off" required>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                    {{Form::submit('Update',array('class'=>'btn btn-success btn-sm', 'style'=>'width:15%'))}}
                                  </div>
                                  {!! Form::close() !!}
                                </div>
                              </div>
                            </div>
                            <!-- End Modal for edit bank -->
                          </td>
                        </tr>
                        @endforeach
                        @if($rowCount==0)
                        <tr>
                          <td colspan="9" align="center">
                            <h4 style="color: #ccc">No Data Found . . .</h4>
                          </td>
                        </tr>
                        @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  {{$alldata->render()}}
                </div>
              
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>

      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"><i class="fa fa-plus-circle"></i> ADD BANK ACCOUNT</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            {!! Form::open(array('route' =>['bank-account.store'],'method'=>'POST')) !!}
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Bank Name</label>
                    <input type="text" name="bank_name" class="form-control" value="" autocomplete="off" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Account Name</label>
                    <input type="text" name="account_name" class="form-control" value="" autocomplete="off" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Account Number</label>
                    <input type="text" name="account_no" class="form-control" value="" autocomplete="off" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Account Type</label>
                    <select class="form-control" name="account_type"> 
                      <option value="">Select</option>
                      @foreach($allaccounttype as $type)
                      <option value="{{$type->id}}">{{$type->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Branch</label>
                    <input type="text" name="bank_branch" class="form-control" value="" autocomplete="off" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Opening Balance</label>
                    <input type="number" name="opening_balance" class="form-control" value="" autocomplete="off" required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label>Date</label>
                    <input type="text" name="opening_date" class="form-control datepicker" value="{{date('Y-m-d')}}" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
              {{Form::submit('Save',array('class'=>'btn btn-success btn-sm', 'style'=>'width:15%'))}}
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
      <!-- ./ Modal -->
    </section>
    <!-- /.content -->
@endsection 