@extends('layouts.layout')
@section('title', 'Amount Transfer')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Amount Transfer</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{URL::To('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">Transfer</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">@include('common.message')</div>
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Transfer</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              {!! Form::open(array('route' =>['amount-transfer.store'],'method'=>'POST')) !!}
                <div class="card-body">
                  <div class="form-group"> 
                    <label>Bank Name</label>
                    <input type="text" class="form-control" value="{{$alldata->bank_name}}" readonly="">
                    <input type="hidden" class="form-control" name="bank_id" value="{{$alldata->id}}">
                  </div>
                  <div class="form-group"> 
                    <label>Account Name</label>
                    <input type="text" class="form-control" value="{{$alldata->account_name}}" readonly="">
                  </div>
                  <div class="form-group"> 
                    <label>Account No</label>
                    <input type="text" class="form-control" value="{{$alldata->account_no}}" readonly="">
                  </div>
                  <div class="form-group"> 
                    <label>Transfer To</label>
                    <select class="form-control" name="transfer_to" required=""> 
                      <option value="">Selcct</option>
                      @foreach($allbank as $bank)
                      <option value="{{$bank->id}}">{{$bank->bank_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group"> 
                    <label>Amount</label>
                    <input type="number" name="transfer_amount" class="form-control" autocomplete="off" required="">
                  </div>
                  <div class="form-group"> 
                    <label>Date</label>
                    <input type="text" name="transaction_date" class="form-control datepicker" value="{{date('Y-m-d')}}">
                  </div>
                  <div class="form-group"> 
                    <label>Note</label>
                    <textarea class="form-control" name="note"></textarea>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              {!! Form::close() !!}
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection 