<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;

    protected $table = "user_types";
    protected $fillable = [
        'user_type', 'user_role'
    ];

    public function userType_user_object()
    {
        return $this->hasOne('App\User', 'type', 'id');
    }
}
