<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";
    protected $fillable = [
        'name', 'bar_code', 'product_type_id', 'product_sub_type_id', 'brand_id', 'unit_id', 'price', 'vat_percent', 'status'
    ];

    public function product_type_object()
    {
        return $this->hasOne('App\Models\ProductType', 'id', 'product_type_id');
    }
    public function product_subtype_object()
    {
        return $this->hasOne('App\Models\ProductSubType', 'id', 'product_sub_type_id');
    }
    public function product_brand_object()
    {
        return $this->hasOne('App\Models\ProductBrand', 'id', 'brand_id');
    }
    public function product_unit_object()
    {
        return $this->hasOne('App\Models\ProductUnit', 'id', 'unit_id');
    }
}
