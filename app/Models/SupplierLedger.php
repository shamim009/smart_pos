<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupplierLedger extends Model
{
    use HasFactory;

    protected $table = "supplier_ledgers";
    protected $fillable = [
        'date', 'bank_id', 'supplier_id', 'amount', 'reason', 'note', 'tok', 'created_by'
    ];

    public function ledger_supplier_object()
    {
        return $this->hasOne('App\Models\Supplier', 'id', 'supplier_id');
    }

    public function ledger_user_object()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
