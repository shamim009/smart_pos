<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockProduct extends Model
{
    use HasFactory;

    protected $table = "stock_products";
    protected $fillable = [
        'product_id', 'quantity', 'unit_price', 'status'
    ];

    public function stockproduct_product_object()
    {
        return $this->hasOne('App\Models\Product', 'id', 'product_id');

    }
}
