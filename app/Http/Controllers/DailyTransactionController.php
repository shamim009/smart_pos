<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trip;
use App\Models\Driver;
use App\Models\RequisitProduct;
use App\Models\TransactionReport;
use App\Models\OtherReceiveVoucher;
use App\Models\OtherPaymentVoucher;
use App\Models\BillCollection;
use Validator;
use Session;
use DB;

class DailyTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['alldata']= TransactionReport::paginate(250);
        return view('accounts.dailyTransaction', $data);
    }

    public function filter(Request $request)
    {
        if ($request->start_date !="" && $request->end_date !="") {
            $data['alldata'] = TransactionReport::whereBetween('transaction_date', [$request->start_date, $request->end_date])->paginate(250);
            $data['start_date'] = $request->start_date;
            $data['end_date'] = $request->end_date;
            
            return view('accounts.dailyTransaction', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function finalReport()
    {
        $data['other_total_received']= OtherReceiveVoucher::sum('amount');
        $data['other_total_payment']= OtherPaymentVoucher::sum('amount');
        $data['bill_collection']= BillCollection::sum('total_bill');
        return view('accounts.finalReport', $data);
    }

    public function finalReportFiltering(Request $request)
    {
        if ($request->start_date !="" && $request->end_date !="") {
            $data['other_total_received']= OtherReceiveVoucher::whereBetween('receive_date', [$request->start_date, $request->end_date])->sum('amount');
            $data['other_total_payment']= OtherPaymentVoucher::whereBetween('payment_date', [$request->start_date, $request->end_date])->sum('amount');
            $data['bill_collection']= BillCollection::whereBetween('issue_date', [$request->start_date, $request->end_date])->sum('total_bill');
            $data['start_date'] = $request->start_date;
            $data['end_date'] = $request->end_date;
            return view('accounts.finalReport', $data);
        }
    }
}
