<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\BankAccount;
use App\Models\SupplierLedger;
use App\Models\TransactionReport;
use App\Models\Transaction;
use Validator;
use Session;
use Auth;
use DB;

class ProductSupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['alldata']= Supplier::paginate(10);
        $data['allbank']= BankAccount::all();
        return view('purchase.supplier', $data);
    }

    public function supplierList()
    {
        $data['alldata']= Supplier::paginate(10);
        $data['allbank']= BankAccount::all();
        return view('purchase.supplierPayment', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->supplier_payment) {
            $validator = Validator::make($request->all(), [
                'pay_amount' => 'required',
                'payment_method' => 'required',
            ]);
            if ($validator->fails()) {
                Session::flash('flash_message', $validator->errors());
                return redirect()->back()->with('status_color','warning');
            }

            DB::beginTransaction();
            try{
                $bug=0;
                // inserting into ledger table
                $insert= SupplierLedger::create([
                    'date'=>date('Y-m-d'),
                    'bank_id'=>$request->payment_method,
                    'supplier_id'=>$request->supplier_id,
                    'amount'=>$request->pay_amount,
                    'reason'=>'payment(supplier)',
                    'note'=>$request->note,
                    'tok'=>date('Ymdhis'),
                    'created_by'=>Auth::id()
                ]);

                // inserting into report table
                $insertIntoReport = TransactionReport::create([
                    'bank_id'=>$request->payment_method,
                    'transaction_date'=>date('Y-m-d'),
                    'reason'=>'payment(supplier)',
                    'amount'=>$request->pay_amount,
                    'tok'=>date('Ymdhis'),
                    'status'=>'1',
                    'created_by'=>Auth::id()
                ]);

                // inserting into transaction table
                $insertIntoTransaction = Transaction::create([
                    'date'=>date('Y-m-d'),
                    'reason'=>'payment(supplier)',
                    'amount'=>$request->pay_amount,
                    'tok'=>date('Ymdhis'),
                    'status'=>'1',
                    'created_by'=>Auth::id()
                ]);

                // update bank amount
                $update=DB::table('bank_accounts')->where('id', $request->payment_method)->decrement('balance', $request->pay_amount);

                DB::commit();
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
                DB::rollback();
            }

            if($bug==0){
                Session::flash('flash_message','Supplier Payment Successfully Done !');
                return redirect()->back()->with('status_color','success');
            }else{
                Session::flash('flash_message','Something Error Found !');
                return redirect()->back()->with('status_color','danger');
            }
        }else{
            $validator = Validator::make($request->all(), [
                'supplier_id' => 'required',
                'name' => 'required',
                'phone' => 'required',
            ]);
            if ($validator->fails()) {
                Session::flash('flash_message', $validator->errors());
                return redirect()->back()->with('status_color','warning');
            }

            $input = $request->all();
            $input['status'] = 1;

            DB::beginTransaction();
            try{
                $bug=0;
                $insert= Supplier::create($input);
                DB::commit();
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
                DB::rollback();
            }

            if($bug==0){
                Session::flash('flash_message','Supplier Successfully Added !');
                return redirect()->back()->with('status_color','success');
            }else{
                Session::flash('flash_message','Something Error Found !');
                return redirect()->back()->with('status_color','danger');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['single_data'] = Supplier::findOrFail($id);
        $data['alldata']= Supplier::paginate(10);
        $data['allbank']= BankAccount::all();
        return view('purchase.supplier', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Supplier::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required',
            'name' => 'required',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            Session::flash('flash_message', $validator->errors());
            return redirect()->back()->with('status_color','warning');
        }
              
        $input = $request->all();

        try{
            $bug=0;
            $data->update($input);
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
        }

        if($bug==0){
            Session::flash('flash_message','Supplier Successfully Updated !');
            return redirect()->back()->with('status_color','warning');
        }else{
            Session::flash('flash_message','Something Error Found !');
            return redirect()->back()->with('status_color','danger');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Supplier::findOrFail($id);
        $action = $data->delete();

        if($action){
            Session::flash('flash_message','Supplier Successfully Deleted !');
            return redirect()->back()->with('status_color','danger');
        }else{
            Session::flash('flash_message','Something Error Found !');
            return redirect()->back()->with('status_color','danger');
        }
    }

    public function supplierLedger(Request $request)
    {
        //$data['alldata']= ProductSell::where('tok', $request->id)->get();
        //$data['singleData']= Vat::where('tok', $request->id)->first();
        $data['alldata']= SupplierLedger::where('supplier_id', $request->id)->get();
        $data['singledata']= Supplier::where('id', $request->id)->first();
        return view('purchase.supplierLedger', $data);
    }

    public function supplierPaymentReport()
    {
        $data['alldata']= SupplierLedger::where('reason', 'like', '%' . 'payment' . '%')->get();
        return view('purchase.supplierPaymentReport', $data);
    }

    public function filter(Request $request)
    {
        if ($request->start_date !="" && $request->end_date !="") {
            $data['alldata']= SupplierLedger::where('reason', 'like', '%' . 'payment' . '%')->whereBetween('date', [$request->start_date, $request->end_date])->get();
            $data['start_date'] = $request->start_date;
            $data['end_date'] = $request->end_date;
        return view('purchase.supplierPaymentReport', $data);
        }
    }
}
