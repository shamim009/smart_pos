<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CustomerLedger;
use Validator;
use Session;
use Response;
use Auth;
use Hash;
use DB;

class CustomerController extends Controller
{
    public function index()
    {
        $data['alldata'] = User::where('user_type', '3')->orderBy('id', 'DESC')->paginate(15);
        return view('customer.index', $data);
    }

    public function customerFilter(Request $request)
    {
        //die('ok');
        $data['alldata'] = User::where('name', 'like', '%' . $request->name . '%')->where('user_type', '3')->orderBy('id', 'DESC')->paginate(15);
        return view('customer.index', $data);
    }

    public function create()
    {
        return view('customer.customer-form');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            //'phone' => 'required|digits:11',
        ]);

        if($validator->fails()){
            $plainErrorText = "";
            $errorMessage = json_decode($validator->messages(), True);
            foreach ($errorMessage as $value) { 
                $plainErrorText .= $value[0].". ";
            }
            Session::flash('flash_message', $plainErrorText);
            return redirect()->back()->withErrors($validator)->withInput()->with('status_color','warning');
        }

        $input = $request->all();
        $input['user_type'] = '3';
        $input['status'] = '1';
        DB::beginTransaction();
        try{
            $bug=0;
            $insert= User::create($input);

            DB::commit();
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            DB::rollback();
        }

        if($bug==0){
            Session::flash('flash_message','Customer Successfully Added. !');
            return redirect()->back()->with('status_color','success');
        }else{
            Session::flash('flash_message','Something Error Found !');
            return redirect()->back()->with('status_color','danger');
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $data = User::findOrfail($id);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            //'phone' => 'required|digits:11',
        ]);

        if($validator->fails()){
            $plainErrorText = "";
            $errorMessage = json_decode($validator->messages(), True);
            foreach ($errorMessage as $value) { 
                $plainErrorText .= $value[0].". ";
            }
            Session::flash('flash_message', $plainErrorText);
            return redirect()->back()->withErrors($validator)->withInput()->with('status_color','warning');
        }
                
        $input = $request->all();
        DB::beginTransaction();
        try{
            $bug=0;
            $data->update($input);
            DB::commit();
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            DB::rollback();
        }

        if($bug==0){
            Session::flash('flash_message','Customer Successfully Updated !');
            return redirect()->back()->with('status_color','warning');
        }else{
            Session::flash('flash_message','Something Error Found.');
            return redirect()->back()->with('status_color','danger');
        }
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);        
        DB::beginTransaction();
        try{
            $bug=0;
            $action = $data->delete();
            DB::commit();
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            DB::rollback();
        }

        if($bug==0){
            Session::flash('flash_message','Customer Successfully Deleted !');
            return redirect()->back()->with('status_color','danger');
        }
        else{
            Session::flash('flash_message','Something Error Found.');
            return redirect()->back()->with('status_color','danger');
        }
    }

    public function customerLedger($id){
        $data['customer_info'] = User::where('id', $id)->first();
        $data['alldata'] = CustomerLedger::where('customer_id', $id)->get();
        return view('customer.ledger', $data);
    }
}
